package firstweekpractic;
/*
     Перевод литров в галлоны. С консоли считывается число n - количество литров, нужно перевести это число в галлоны.
     (1 литр = 0,219969 галлона)
     */

import java.util.Scanner;

public class Task7<DOUBLELITERS_INGALON> {
    static final double LITERS_INGALON = 0.219969;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double res = n * LITERS_INGALON;
        System.out.println("Результат " + res);
    }
}
