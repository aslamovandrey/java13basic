package firstweekpractic;
/*
Дано целое число n.
Выведите следующее за ним четное число.
При решении этой задачи нельзя использовать условную инструкцию if и циклы.

5 -> 6
10 -> 12
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int res = a + 2 - (a & 1);
        int res2 = (a / 2 + 1) * 2;
        System.out.println("Результат " + res);
        System.out.println("Результат2 " + res2);
    }
}
