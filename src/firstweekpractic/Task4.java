package firstweekpractic;
/*

Дана площадь круга, нужно найти диаметр окружности и длину окружности.
S  = PI * (D^2 / 4) - это через диаметр => d = sqrt(S * 4 / PI)
S = PI * r^2 - радиус
S = L^2 / (4 *PI) - площадь через длину
Отношение длины окружности к диаметру является постоянным числом.
π = L : d

Входные данные:
91
 */

import java.util.Scanner;

import static java.lang.Math.*;

public class Task4 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();
        double d = Math.sqrt(s * 4 / Math.PI);
        double l = Math.PI * d;
        System.out.println(d);
        System.out.println(l);
    }
}
