package week9.oop3.arrlist;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("BMW", "1990"));
        cars.add(new Car("VOLVO", "1991"));
        cars.add(new Car("VOLVO", "1991"));
        cars.add(new Car("VOLVO", "1991"));
        cars.add(new Car("MAZDA", "2022"));

//        Set<Car> carSet = new HashSet<>(cars);
//        carSet.forEach(System.out::println);

        for (Car car : cars) {
            System.out.println(car.getModel() + " " + car.getYear());
        }

        cars.forEach(System.out::println);


//        java.util.ConcurrentModificationException:
//        for (Car car : cars) {
//            if (car.getModel().equals("MAZDA")) {
//                cars.remove(car);
//            }
//        }

        Iterator<Car> iterator = cars.iterator();
        while (iterator.hasNext()) {
            Car car = iterator.next();
            if (car.getModel().equals("MAZDA")) {
                iterator.remove(); //cars.remove(car);
            }
        }
        cars.forEach(System.out::println);

        Collections.sort(cars, new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getModel().compareTo(o2.getModel());
            }
        });

        Collections.sort(cars, (o1, o2) -> o1.getModel().compareTo(o2.getModel()));

        Collections.sort(cars, Comparator.comparing(Car::getModel));
    }
}

