package week8.oop2.Tasc1;

public class VariableLength {

    static int sum(int... numbers) {
        //int[] numbers
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        return sum;
    }

    static boolean findChar(Character ch, String... strings) {
        for (String string : strings) {
            if (string.indexOf(ch) != -1) {
                return true;
            }
        }
        return false;
    }

    public static void main(String... args) {
        System.out.println(sum(1, 2, 3, 5, 4, 6));
        System.out.println(findChar('a', "python", "java"));

        int a = 123;
        System.out.println("" + a);
        System.out.println(String.format("This is an integer: %d", 123));
        System.out.println(String.format("This is an integer: %s %d", "asd", 123));
        String test = """
              select adlakdnkjasbfljhsbflkasjdfn;ksd
              """;
        //StringBuilder stringBuilder = new StringBuilder();
        // stringBuilder.append("ad").append("addased").reverse();
    }
}


