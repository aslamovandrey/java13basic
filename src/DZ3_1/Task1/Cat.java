package DZ3_1.Task1;
/*
 Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
● sleep() — выводит на экран “Sleep”
● meow() — выводит на экран “Meow”
● eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */
import java.util.Random;

public class Cat {
    private void sleep(){
        System.out.println("Sleep");
    }

    private void meow(){
        System.out.println("Meow");
    }

    private void eat(){
        System.out.println("Eat");
    }


    //status() — вызывает один из приватных методов случайным образом.
    public void status() {
        Random gen = new Random();
        switch (gen.nextInt(3)){
            case 0 -> meow();
            case 1 -> sleep();
            case 2 -> eat();
        }
    }
}
