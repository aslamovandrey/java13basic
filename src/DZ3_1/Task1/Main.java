package DZ3_1.Task1;
/*
Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
● sleep() — выводит на экран “Sleep”
● meow() — выводит на экран “Meow”
● eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом
 */
public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.status();

    }
}
