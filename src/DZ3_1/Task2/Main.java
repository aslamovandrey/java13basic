package DZ3_1.Task2;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
 */
import java.util.Arrays;

import static DZ3_1.Task3.StudentService.bestStudent;
import static DZ3_1.Task3.StudentService.sortBySurname;

public class Main {
    public static void main(String[] args) {

        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        student1.setSurname("Blanc");
        student2.setSurname("Adams");
        student3.setSurname("Green");

        int[] arr1 = {3,4,5,5,3,3,2,4};
        int[] arr2 = {3,4,3,5,5,3,2,4,3};
        int[] arr3 = {3,3,3,5,4,3,2,4,5,2};

        student1.setGrades(arr1);
        student2.setGrades(arr2);
        student3.setGrades(arr3);

        Student[] students = {student1,student2,student3};

        sortBySurname(students);

        for (Student stud : students) {
            System.out.println(stud.toString());
        }

        System.out.println( bestStudent(students).toString());



    }
}
