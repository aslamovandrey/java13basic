package DZ3_1.Task2;

import java.util.Arrays;
import java.util.OptionalDouble;

public class Student {
    //приватные поля:
    private String name;
    private String surname;
    private int[] grades;

    //геттеры/сеттеры
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades.length > 10 ? null : grades;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }

    //метод, добавляющий новую оценку в grades
    public void addNewGrade(int grade) {
        for (int i = 0; i < this.grades.length; i++) {
            if (i == this.grades.length - 1) {
                this.grades[i] = grade;
            } else {
                this.grades[i] =  this.grades[i + 1];
            }
        }

    }
    //метод, возвращающий средний балл студента
    public Double getRating() {

        return (int)(Arrays.stream(this.grades).average().orElse(Double.NaN) * 100) / 100.0;
    }
}
