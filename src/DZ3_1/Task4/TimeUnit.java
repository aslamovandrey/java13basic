package DZ3_1.Task4;
/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).

 */
public class TimeUnit {

    private int hour = 0;
    private int minute = 0;
    private int second = 0;

    private static boolean isValidTime(int hour, int minute, int second){
        return  hour >= 0 && minute >= 0 && second >= 0 && hour < 24 && minute < 60 && second < 60;
    }

    public TimeUnit (int hour, int minute, int second){
        if (isValidTime( hour, minute, second)) {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        } else {
            System.out.println("Не верный формат времени!");
        }
    }
    public TimeUnit (int hour, int minute){
        if (isValidTime( hour, minute, this.second)) {
            this.hour = hour;
            this.minute = minute;
        } else {
            System.out.println("Не верный формат времени!");
        }
    }
    public TimeUnit (int hour){
        if (isValidTime( hour, this.minute, this.second)) {
            this.hour = hour;
        } else {
            System.out.println("Не верный формат времени!");
        }
    }

    public void printTime(){
        System.out.println("Установленное время: " + (this.hour > 10 ? this.hour : "0" + this.hour) + ":" +
                (this.minute > 10 ? this.minute : "0" + this.minute) + ":" +
                (this.second > 10 ? this.second : "0" + this.second));

    }
    public void printTimeAmPm(){
        int hourAmPm = this.hour % 12;
        String amPm = this.hour > 12 ? "pm" : "am";
        System.out.println("Установленное время: " + (hourAmPm > 10 ? hourAmPm : "0" + hourAmPm) + ":" +
                (this.minute > 10 ? this.minute : "0" + this.minute) + ":" +
                (this.second > 10 ? this.second : "0" + this.second) +
                " " + amPm );

    }

    public void timePlusTime (int hour, int minute, int second) {
        if (isValidTime( hour, minute, second)) {
            this.hour = (this.hour + hour + (this.minute + minute) / 60 ) % 24;
            this.minute = (this.minute + minute + (this.second + second) / 60 ) % 60;
            this.second = (this.second + second) % 60;
        } else {
            System.out.println("Не верный формат прибавляемого времени!");
        }
    }

}
