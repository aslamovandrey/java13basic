package DZ3_1.Task8;

public class Atm {
    private double rubToUsd = 1;
    private double usdToRub = 1;

    private static int counter = 0;

    public Atm (double currentToValute, ValutToConvrtation valutToConvrtation) {
        counter++;
        if(valutToConvrtation == ValutToConvrtation.DOLLAR_PER_RUBLES){
            usdToRub = currentToValute;
        }
        if (valutToConvrtation == ValutToConvrtation.RUBLES_PER_DOLLAR){
            rubToUsd = currentToValute;
        }
        else {
            System.out.println("Валюта не распознана. По умолчанию курс Рубли в Доллары");
            rubToUsd = currentToValute;
        }
    }

    public double convertToDollar(double currentValue) {
        return (int)(currentValue / rubToUsd * 100) / 100.0 ;
    }
    public double convertToRubles(double currentValue) {
        return (int)(currentValue * usdToRub * 100) / 100.0 ;
    }

    public static int getCounter() {
        return counter;
    }

}
