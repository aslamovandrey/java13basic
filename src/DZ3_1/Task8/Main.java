package DZ3_1.Task8;

public class Main {
    public static void main(String[] args) {
        Atm atm1 = new Atm(61.31, ValutToConvrtation.RUBLES_PER_DOLLAR);
        Atm atm2 = new Atm(0.016, ValutToConvrtation.DOLLAR_PER_RUBLES);

        System.out.println(atm1.convertToDollar(10000));
        System.out.println(atm2.convertToRubles(10000));

        System.out.println(Atm.getCounter());

    }
}
