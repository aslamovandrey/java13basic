package DZ3_1.Task3;
/*
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
 */
import DZ3_1.Task2.Student;

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {
    public static Student bestStudent(Student[] studentsArr) {
        double bestRating = 0;
        int bestStudentIdx = 0;

        for (int i = 0; i < studentsArr.length; i++) {
            if (studentsArr[i].getRating() >= bestRating) {
                bestRating = studentsArr[i].getRating();
                bestStudentIdx = i;
            }
        }
        return studentsArr[bestStudentIdx];
    }

    public static Student[] sortBySurname(Student[] studentsArr){
        Arrays.sort(studentsArr, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getSurname().compareTo(o2.getSurname());
            }
        });
        return studentsArr;
    }

}
