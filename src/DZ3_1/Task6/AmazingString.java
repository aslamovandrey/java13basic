package DZ3_1.Task6;

import java.util.Arrays;

public class AmazingString {
    private char[] arreyChar;

    public AmazingString(char[] arreyChar) {
        this.arreyChar = arreyChar;
    }
    public AmazingString(String string) {
        int n = string.length();
        arreyChar = new char[n];
        for (int i = 0; i < n; i++) {
            arreyChar[i] = string.charAt(i);
        }
    }

    public char getSymbolOfIndex (int idx) {
        return arreyChar[idx];
    }

    public int setLenghtStr(){
        return arreyChar.length;
    }

    public void printAmazingString(){
        for (int i = 0; i < arreyChar.length; i++) {
            System.out.print(arreyChar[i]);
        }
        System.out.println("");
    }

    public boolean subStrAmazingString(char[] subArreyChar) {
        boolean flag = false;
        for (int i = 0; i < arreyChar.length ; i++) {
            if (arreyChar[i] == subArreyChar[0]) {
                char[] tempArr = new char[subArreyChar.length];
                System.arraycopy(arreyChar,i,tempArr,0,subArreyChar.length);
                if(Arrays.equals(subArreyChar,tempArr)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    public boolean subStrAmazingString(String subString) {
        int n = subString.length();
        char[] subArreyChar = new char[n];
        for (int i = 0; i < n; i++) {
            subArreyChar[i] = subString.charAt(i);
        }
        
        boolean flag = false;
        for (int i = 0; i < arreyChar.length ; i++) {
            if (arreyChar[i] == subArreyChar[0]) {
                char[] tempArr = new char[subArreyChar.length];
                System.arraycopy(arreyChar,i,tempArr,0,subArreyChar.length);
                if(Arrays.equals(subArreyChar,tempArr)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    public void removeSpace(){
        int countSpace = 0;
        for (char ch : this.arreyChar) {
            if (ch == ' ') {
                countSpace++;
            } else break;
        }
        char[] tempArr = new char[this.arreyChar.length - countSpace];
        System.arraycopy(arreyChar,countSpace,tempArr,0,tempArr.length);
        this.arreyChar = tempArr;
    }

    public void reversAmazingString(){
        for (int i = 0; i < this.arreyChar.length / 2; i++) {
            char temp = this.arreyChar[i];
            this.arreyChar[i] = this.arreyChar[this.arreyChar.length - 1 - i];
            this.arreyChar[this.arreyChar.length - 1 - i] = temp;
        }
    }

}
