package DZ3_1.Task6;

public class Main {
    public static void main(String[] args) {

        char[] arr1 = {' ','a','b','c','d'};
        String str = "qwerty";

        AmazingString amazingString1 = new AmazingString(arr1);
        AmazingString amazingString2 = new AmazingString(str);

        System.out.println(amazingString1.getSymbolOfIndex(2));
        System.out.println(amazingString2.getSymbolOfIndex(3));

        amazingString1.removeSpace();
        amazingString2.reversAmazingString();

        System.out.println(amazingString1.setLenghtStr());
        System.out.println(amazingString2.setLenghtStr());

        amazingString1.printAmazingString();
        amazingString2.printAmazingString();

        System.out.println(amazingString1.subStrAmazingString(new char[]{'c', 'x'}));
        System.out.println(amazingString1.subStrAmazingString("cd"));


    }
}
