package DZ3_1.Task5;

public class Main {
    public static void main(String[] args) {
        DayOfWeek[] arrayDayOfWeek = new DayOfWeek[7];

        DayOfWeek dayOfWeek1 = new DayOfWeek((byte) 1,"Monday");
        DayOfWeek dayOfWeek2 = new DayOfWeek((byte) 2,"Tuesday");
        DayOfWeek dayOfWeek3 = new DayOfWeek((byte) 3,"Wednesday");
        DayOfWeek dayOfWeek4 = new DayOfWeek((byte) 4,"Thursday");
        DayOfWeek dayOfWeek5 = new DayOfWeek((byte) 5,"Friday");
        DayOfWeek dayOfWeek6 = new DayOfWeek((byte) 6,"Saturday");
        DayOfWeek dayOfWeek7 = new DayOfWeek((byte) 7,"Sunday");


        arrayDayOfWeek[0] = dayOfWeek1;
        arrayDayOfWeek[1] = dayOfWeek2;
        arrayDayOfWeek[2] = dayOfWeek3;
        arrayDayOfWeek[3] = dayOfWeek4;
        arrayDayOfWeek[4] = dayOfWeek5;
        arrayDayOfWeek[5] = dayOfWeek6;
        arrayDayOfWeek[6] = dayOfWeek7;

        for (DayOfWeek dayOfWeek : arrayDayOfWeek) {
            System.out.println(dayOfWeek.getNumberOfDay() + " " + dayOfWeek.getNameOfDay());
        }

    }
}
