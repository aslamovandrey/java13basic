package PM.DZ3.Task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
Есть класс APrinter:
public class APrinter {
public void print(int a) {
System.out.println(a);
}
}
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки
 */
public class MainReflection {
    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();
        Class cls = aPrinter.getClass();

        try {
            Method method = cls.getMethod("print", int.class);
            method.invoke(aPrinter,100500);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
