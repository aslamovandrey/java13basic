package PM.DZ3.Task2;

import PM.DZ3.Task1.IsLike;
import PM.DZ3.Task1.MyClass;
;

public class Main {
    public static void main(String[] args) {
        printAnotstion(MyClass.class);
    }
    public static void printAnotstion (Class<?> clazz) {
        if (!clazz.isAnnotationPresent(IsLike.class)) {
            return;
        }
        IsLike isLike = clazz.getAnnotation(IsLike.class);
        System.out.println("isLike: " + isLike.isLike());
    }
}
