package PM.DZ3.Task5;

public class RoundBrackets {
    public static void main(String[] args) {
        System.out.println("(()()()) " + checkRoundBrackets("(()()())"));
        System.out.println(")( " + checkRoundBrackets(")("));
        System.out.println("(() " + checkRoundBrackets("(() "));
        System.out.println("((())) " + checkRoundBrackets("((()))"));
    }

    private static boolean checkRoundBrackets(String s) {

        int tmp = 0;
        for (int i = 0; i < s.length(); i++) {
            //System.out.println(strB.substring(i,i+1) + tmp);
            if (tmp < 0) break;
            if (s.substring(i,i+1).equals("(")) {
                tmp++;

            }
            if (s.substring(i,i+1).equals(")")) {
                tmp--;
            }
        }
        //System.out.println( tmp);

        return tmp == 0 ? true : false;
    }

}
