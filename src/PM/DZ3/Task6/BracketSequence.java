package PM.DZ3.Task6;

public class BracketSequence {
    public static void main(String[] args) {
        System.out.println("{()[]()} " + checkRBracketSequence("{()[]()}"));
        System.out.println("{)(} " + checkRBracketSequence("{)(}"));
        System.out.println("[} " + checkRBracketSequence("[}"));
        System.out.println("[{(){}}][()]{}  " + checkRBracketSequence("[{(){}}][()]{} "));
    }

    private static boolean checkRBracketSequence(String s) {

        int tmpRound = 0;
        int tmpSquare = 0;
        int tmpCurly = 0;
        for (int i = 0; i < s.length(); i++) {
            //System.out.println(strB.substring(i,i+1) + tmp);
            if (tmpRound < 0 || tmpSquare < 0 || tmpCurly < 0) break;
            switch (s.substring(i,i+1)) {
                case "(" -> tmpRound++;
                case ")" -> tmpRound--;
                case "{" -> tmpCurly++;
                case "}" -> tmpCurly--;
                case "[" -> tmpSquare++;
                case "]" -> tmpSquare--;
            }
        }
        //System.out.println( tmp);

        return tmpRound == 0 && tmpSquare == 0 && tmpCurly == 0 ? true : false;
    }

}
