package PM.DZ3.Task4;

import DZ3_3.task1.Eagle;

import java.util.*;

/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */
public class ReflectionAllInterfaces {
    public static void main(String[] args) {

        List<Class<?>> result = getAllInterfaces(ArrayList.class);
        for (Class<?> anInterface : result) {
            System.out.println(anInterface.getName());
        }

    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();

        }
        return interfaces;
    }
}
