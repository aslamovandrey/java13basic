package PM.DZ3.Task1;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/*
Создать аннотацию @IsLike, применимую к классу во время выполнения
программы. Аннотация может хранить boolean значение.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface IsLike {
    boolean isLike();
}
