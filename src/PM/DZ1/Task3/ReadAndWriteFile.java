package PM.DZ1.Task3;

import java.io.*;
import java.util.Scanner;

public class ReadAndWriteFile {
    private static final String FOLDER_DIRECTORY = "C:\\Users\\afasl\\IdeaProjects\\Java13_2\\src\\PM\\DZ1\\Task3\\Files";
    private static final String INPUT_FILE = "\\input.txt";
    private static final String OUTPUT_FILE = "\\output.txt";

    private ReadAndWriteFile() {

    }

    public static void readAndWriteData() {
        try (Scanner scanner = new Scanner(new File(FOLDER_DIRECTORY + INPUT_FILE));
             Writer writer = new FileWriter(FOLDER_DIRECTORY + OUTPUT_FILE)){
            while (scanner.hasNextLine()) {
                writer.write(upperSimblInLowerCase(scanner.nextLine())+ "\n");
            }
        } catch (IOException e) {
            System.out.println("ReadAndWriteFile!error: " + e.getMessage());
        }
    }
    private static String upperSimblInLowerCase (String str) {
        StringBuilder builder = new StringBuilder();
        for (char ch : str.toCharArray()){
            ch = (ch >= 97 && ch <= 122) ? Character.toUpperCase(ch) : ch;
            builder.append(ch);
        }
        return builder.toString();
    }

}
