package PM.DZ1.Task3;
/*
Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
./output.txt текст из input, где каждый латинский строчный символ заменен на
соответствующий заглавный. Обязательно использование try с ресурсами.
 */
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        ReadAndWriteFile.readAndWriteData();
    }
}
