package PM.DZ1.Task4;

public class MyEvenNumber {
    private int n;

    public int getN() {
        return n;
    }

    public void setN(int n) throws MyNoEvenNumberException {
        if(n%2 != 0) throw new MyNoEvenNumberException("По заданию в MyEvenNumber не должно быть нечетных чисел");
        this.n = n;
    }

    public MyEvenNumber(int n) throws MyNoEvenNumberException {
        this.setN(n);
    }
}
