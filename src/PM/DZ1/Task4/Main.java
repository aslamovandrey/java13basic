package PM.DZ1.Task4;

public class Main {
    public static void main(String[] args) throws MyNoEvenNumberException {

        MyEvenNumber men1 = new MyEvenNumber(2);
        try {
            MyEvenNumber men2 = new MyEvenNumber(3);
        } catch (MyNoEvenNumberException e) {
            System.out.println("Исключение " + e.getMessage());
        }
        MyEvenNumber men3 = new MyEvenNumber(4);
        System.out.println(men3.getN());
    }
}
