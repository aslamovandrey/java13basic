package PM.DZ1.Task8;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int p = scanner.nextInt();
        int iElement = -1;

        for (int i = 0; i < n; i++) {
            if (p == arr[i]) iElement =i;
        }
        System.out.println(iElement);
    }
}
