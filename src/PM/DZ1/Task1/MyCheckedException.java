package PM.DZ1.Task1;
/*
Создать собственное исключение MyCheckedException, являющееся
проверяемым.
 */
public class MyCheckedException extends Exception{
    public MyCheckedException (String message){
        super(message);
    }
}
