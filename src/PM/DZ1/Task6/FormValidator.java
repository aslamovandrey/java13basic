package PM.DZ1.Task6;

import java.text.SimpleDateFormat;
import java.util.Date;


public class FormValidator {
    //длина имени должна быть от 2 до 20
    //символов, первая буква заглавная
    public static void checkName(String str) throws Exception {
        boolean b = (str.matches("^[A-ZА-Я][a-zа-я]{2,20}$"));
        if (!b) {
            throw new Exception("FormValidator#checkName: Ошибка валидации");
        }
    }
    //дата рождения должна быть не
    //раньше 01.01.1900 и не позже текущей даты.
    public static void checkBirthdate(String str) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

        if (formatter.parse(str).before(formatter.parse("01.01.1900")) | formatter.parse(str).after(new Date())){
            throw new Exception("FormValidator#checkBirthdate: Ошибка валидации");
        }

    }
    //пол должен корректно матчится в
    //enum Gender, хранящий Male и Female значения
    public static void checkGender(String str) throws Exception {
        boolean b = Gender.MALE.gender.equals(str) | Gender.FEMALE.gender.equals(str);
        if (!b) {
            throw new Exception("FormValidator#checkGender: Ошибка валидации");
        }
    }
    //рост должен быть положительным
    //числом и корректно конвертироваться в double
    public static void checkHeight(String str) throws Exception {
        //boolean b = Double.parseDouble(str) > 0;
        if (Double.parseDouble(str) < 0) {
            throw new Exception("FormValidator#checkHeight: Ошибка валидации");
        }
    }
}

