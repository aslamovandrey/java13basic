package PM.DZ1.Task6;

public enum Gender {
    MALE("Male"),
    FEMALE("Female");

    public final String gender;
    Gender(String gender) {
        this.gender = gender;
    }
}
