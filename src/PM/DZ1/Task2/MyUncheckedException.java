package PM.DZ1.Task2;
/*
Создать собственное исключение MyUncheckedException, являющееся
непроверяемым.
 */
public class MyUncheckedException extends RuntimeException{
    public MyUncheckedException(String message){
        super(message);
    }
}
