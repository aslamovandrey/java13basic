package PM.DZ2.Task5_dop;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("waaa");
        words.add("waaa");
        words.add("aaa");
        words.add("aaa");
        words.add("bbb");
        words.add("bbb");
        words.add("ddd");
        words.add("ddd");
        words.add("ccc");
        words.add("bbb");
        words.add("ccc");
        words.add("ccc");
        words.add("day");
        words.add("wey");
        words.add("wey");

        int k = 4;

        System.out.println(frequentlyOccurringWords(words,k));
    }

    private static List<String> frequentlyOccurringWords(List<String> words, int k) {
        //TreeMap сразу сортирует по индексу
        Map<String, Integer> tmpMap = new TreeMap<>();
        List<String> res = new ArrayList<>();
        for (String str : words){
            int cou = 1;
            if (tmpMap.containsKey(str)) {
                cou = tmpMap.get(str);
                cou++;
            }
            tmpMap.put(str, cou);
        }

        //Collections.sort не работает с Map
        List<Map.Entry<String, Integer>> arr = new ArrayList<>();
        for (Map.Entry<String, Integer> e : tmpMap.entrySet()) {
            arr.add(e);
        }
        Collections.sort(arr, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return -1 * o1.getValue().compareTo(o2.getValue());
            }
        });

        //System.out.println(arr);

        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> e : arr) {
            sortedMap.put(e.getKey(), e.getValue());
        }

        //System.out.println(sortedMap);

        int i = 0;
        for (Map.Entry<String, Integer> e : sortedMap.entrySet()) {
            i++;
            res.add(e.getKey());
            if (i==k){
                break;
            }

        }
        //System.out.println(res);

        return res;
    }
}
