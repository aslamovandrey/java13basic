package PM.DZ2.Task4;
/*
В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Document> list = new ArrayList<>();
        list.add(new Document(1,"Устав", 54));
        list.add(new Document(2,"Договор купли-продажи", 10));
        list.add(new Document(3,"Акт приема-передачи", 3));
        list.add(new Document(4,"Счет-фактура", 1));

        Map<Integer, Document> newHM = organizeDocuments(list);

        System.out.println("Элементы HashMap: " + newHM);
        System.out.println("Элемент HashMap с id=1: " +newHM.get(1));

    }

//    Реализовать метод со следующей сигнатурой:
//    public Map<Integer, Document> organizeDocuments(List<Document> documents)
    public static Map<Integer, Document> organizeDocuments(List<Document> documents){
        Map<Integer, Document> hm = new HashMap<>();
        for (Document doc: documents){
            hm.put(doc.getId(),doc);
        }
        return hm;
    }
}
