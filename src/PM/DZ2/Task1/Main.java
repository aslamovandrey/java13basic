package PM.DZ2.Task1;
/*Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
*/
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

//Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
//набор уникальных элементов этого массива. Решить используя коллекции.
public class Main {
    public static <T> void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Java");
        list.add("Hello");
        list.add("World");
        list.add(list.get(0).substring(0,2) + "va");
        System.out.println(list);
        System.out.println(returnUnicuElement(list));

        ArrayList<Integer> list2= new ArrayList<>();
        list2.add(1);
        list2.add(1);
        list2.add(2);
        list2.add(3);
        System.out.println(list2);
        System.out.println(returnUnicuElement(list2));
    }
    public static <T> Set<T> returnUnicuElement(ArrayList<T> list) {
        Set<T> set = new HashSet<>();
        for (T element : list) {
            set.add(element);
        }

        return set;
    }


}
