package PM.DZ2.Task3;

import java.util.HashSet;
import java.util.Set;

/*Реализовать класс PowerfulSet, в котором должны быть следующие методы:
*/
public class PowerfulSet {
/*public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {1, 2}
 */
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2){
        Set<T> set = new HashSet<>(set1);
        set.retainAll(set2);
        return set;
    }
/*
public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {0, 1, 2, 3, 4}
 */
    public static <T> Set<T> union(Set<T> set1, Set<T> set2){
        Set<T> set = new HashSet<>(set1);
        set.addAll(set2);
        return set;
    }


    /*
    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
возвращает элементы первого набора без тех, которые находятся также
и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
     */

    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>(set1);
        set.removeAll(set2);
        return set;
    }

    private PowerfulSet(){

    }
}
