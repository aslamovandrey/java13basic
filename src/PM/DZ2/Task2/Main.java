package PM.DZ2.Task2;
/*С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.

 */
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();
        String str2 = scanner.nextLine();
        System.out.println( "isAnagram: " + isAnagram(str1, str2));
    }

    private static boolean isAnagram(String str1, String str2) {
        char[] charArr1 = str1.replace(" ","").toCharArray();
        char[] charArr2 = str2.replace(" ","").toCharArray();

        if (charArr1.length != charArr2.length) {
            return false;
        }
        Arrays.sort(charArr1);
        Arrays.sort(charArr2);

        return Arrays.equals(charArr1,charArr2);
    }
}
