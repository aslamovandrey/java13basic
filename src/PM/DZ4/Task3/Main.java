package PM.DZ4.Task3;

import java.util.List;
import java.util.Optional;

/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3
 */
public class Main {
    public static void main(String[] args) {
        List<String> numbers = List.of("abc", "", "", "def", "qqq");
        long result =
                numbers.stream()
                        .filter((s -> s != ""))
                        .count();
        System.out.println(result);
    }
}
