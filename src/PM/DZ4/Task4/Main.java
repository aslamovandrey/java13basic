package PM.DZ4.Task4;

import java.util.List;

/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(-1, 7, 67, -4, 5);
        List result = numbers.stream()
                .sorted().toList();
        System.out.println(result);
    }
}
