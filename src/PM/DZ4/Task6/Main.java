package PM.DZ4.Task6;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */
public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = Set.of(1, 3, 5);
        Set<Integer> set2 = Set.of(2, 4, 6);
        Set<Set<Integer>> set = new HashSet<>();
        set.add(set1);
        set.add(set2);
        System.out.println(transInSet(set));

    }
    public static Set<Integer> transInSet (Set<Set<Integer>> inSet){
        //Set<Integer> outSet = new HashSet<>();
        return inSet.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());

    }
}
