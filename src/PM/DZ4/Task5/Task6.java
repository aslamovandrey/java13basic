package PM.DZ4.Task5;

import java.util.List;
import java.util.stream.Collectors;

/*
На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ
 */
public class Task6 {
    public static void main(String[] args) {
        List<String> numbers = List.of("abc", "def", "qqq");
        String res = numbers.stream()
                .map(String::toUpperCase)
//                .map(s -> s + ", ")
                .collect(Collectors.joining(","));

        System.out.println(res);
    }
}
