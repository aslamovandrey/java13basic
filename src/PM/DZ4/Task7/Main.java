package PM.DZ4.Task7;

import java.util.ArrayList;
import java.util.List;

/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("“cat” “cats” -> " + checkStringsForChange("cat", "cats"));
        System.out.println("“cat” “cut” -> " + checkStringsForChange("cat", "cut"));
        System.out.println("“cat” “nut” -> " + checkStringsForChange("cat", "nut"));
        System.out.println("“cat” “at” -> " + checkStringsForChange("cat", "at"));

    }
    public static boolean checkStringsForChange(String s1, String s2){
        List<Character> leftStr = new ArrayList<>(s1.chars()
                .mapToObj(ch -> (char) ch)
                .toList());
        List<Character> rightStr = new ArrayList<>(s2.chars()
                .mapToObj(ch -> (char) ch)
                .toList());
        if (s1.length()<= s2.length()) {
            leftStr.forEach(rightStr::remove);
            //System.out.println(rightStr.size());
            return rightStr.size() == 1;
        } else {
            rightStr.forEach(leftStr::remove);
            return leftStr.size() == 1;
        }


    }
}
