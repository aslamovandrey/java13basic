package PM.DZ4.Task2;

import java.util.List;
import java.util.Optional;

/*
На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);
        Optional<Integer> result =
        numbers.stream()
                .reduce((sum,x) -> sum*x);
        System.out.println(result.get());

    }
}
