package PM.DZ4.Task1;

import java.util.stream.IntStream;

/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(sumEvenNum(1,100));
    }
    public static int sumEvenNum(int x, int y) {
        return IntStream
                .rangeClosed(x, y)
                .filter((p) -> p % 2 == 0)
                .sum();
    }
}
