--1. Sql-скрипты создания таблиц с необходимыми связями

--shop price
create table shop_price
(   id       serial primary key,
    name     varchar(30) not null,
    price    money not null,
    currency varchar(30) not null
);

--customer
create table customer
(   id      serial primary key,
    fio     varchar(250) not null,
    phone   varchar(20) not null
);

--orders
create table orders
(   id          serial primary key,
    price_id    integer REFERENCES shop_price (id),
    customer_id integer REFERENCES customer (id),
    date_added  timestamp,
    amount      integer not null,
    total_price integer not null
);

--select * from customer;
