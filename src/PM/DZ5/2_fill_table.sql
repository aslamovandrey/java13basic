/**
Sql-скрипты наполнения таблиц (тестово добавить несколько строк данных для
последующей проверки select’ов)
*/

--shop price
insert into shop_price(name, price, currency)
values ('Розы', 100, 'Золотые монеты');
insert into shop_price(name, price, currency)
values ('Лилии', 50, 'Золотые монеты');
insert into shop_price(name, price, currency)
values ('Ромашки', 25, 'Золотые монеты');

--select * from orders;

--customer

insert into customer(fio, phone)
values ('Иванов И.И.', '+7(987)654-32-10');
insert into customer(fio, phone)
values ('Петров П.П.', '+7(789)456-23-01');
insert into customer(fio, phone)
values ('Сидоров С.С.', '+7(012)345-67-89');

--orders
insert into orders(price_id, customer_id, date_added, amount, total_price)
values (1, 1, now() - interval '6d', 21, 21*100);
insert into orders(price_id, customer_id, date_added, amount, total_price)
values (2, 1, now() - interval '5d', 3, 3*50);
insert into orders(price_id, customer_id, date_added, amount, total_price)
values (3, 1, now() - interval '4d', 5, 5*25);

insert into orders(price_id, customer_id, date_added, amount, total_price)
values (1, 2, now() - interval '3d', 9, 9*100);
insert into orders(price_id, customer_id, date_added, amount, total_price)
values (3, 2, now() - interval '2d', 7, 7*25);

insert into orders(price_id, customer_id, date_added, amount, total_price)
values (2, 3, now() - interval '1d', 7, 7*50);
insert into orders(price_id, customer_id, date_added, amount, total_price)
values (3, 3, now(), 7, 7*25);