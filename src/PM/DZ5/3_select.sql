--1. По идентификатору заказа получить данные заказа и данные клиента,
--создавшего этот заказ
select *
from orders o
join customer c on o.customer_id = c.id
where o.id = 4

--2. Получить данные всех заказов одного клиента по идентификатору
--клиента за последний месяц
select o.*
from orders o
join customer c on o.customer_id = c.id
where c.id = 1
and o.date_added >= now() - interval '1 months';
--select now() - interval '1 months'

--3. Найти заказ с максимальным количеством купленных цветов, вывести их
--название и количество
select
p.name as "Название цветов",
o.amount as "Количество"
from orders o
join shop_price p on o.price_id = p.id
order by o.amount desc
limit 1

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
--время
select sum(total_price) as "Общая выручка за все время"
from orders;
