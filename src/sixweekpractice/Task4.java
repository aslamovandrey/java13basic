package sixweekpractice;
/*
   Написать функцию через рекурсию для вычисления суммы заданных положительных целых чисел a b
   без прямого использования оператора +.
    */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println( sum(a,b));
    }
    private static int sum (int a, int b){
        if (b ==  0) {
            return a;
        }
        return sum(++a,--b);
    }

}
