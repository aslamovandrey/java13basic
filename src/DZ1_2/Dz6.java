package DZ1_2;

import java.util.Scanner;

public class Dz6 {
    public static void main(String[] args) {
        int count = new Scanner(System.in).nextInt();
        if (count < 500) {
            System.out.println("beginner");
        } else if (count < 1500) {
            System.out.println("pre-intermediate");
        } else if (count < 2500) {
            System.out.println("intermediate");
        } else if (count < 3500) {
            System.out.println("upper-intermediate");
        } else
            System.out.println("fluent");
    }
}
