package DZ1_2;

import java.util.Scanner;

public class Dz11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();

        if (a >= b && a >= c){
            System.out.println(b + c > a ? true : false);
        } else if (b >= a && b >= c) {
            System.out.println(a + c > b ? true : false);
        } else
            System.out.println(a + b > c ? true : false);
    }
}
