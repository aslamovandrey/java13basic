package DZ1_2;

import java.util.Scanner;

public class Dz_dop2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String postalParcel = scanner.nextLine();
        if (postalParcel.matches(".*(камни!).*(запрещенная продукция).*")) System.out.println("в посылке камни и запрещенная продукция");
        else if (postalParcel.matches(".*(камни!).*")) System.out.println("камни в посылке");
        else if (postalParcel.matches(".*(запрещенная продукция).*")) System.out.println("в посылке запрещенная продукция");
        else System.out.println("все ок");
    }
}
