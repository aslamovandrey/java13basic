package DZ1_2;

import java.util.Scanner;

public class Dz9 {
    public static void main(String[] args) {
        double x = new Scanner(System.in).nextDouble();
        System.out.println( (Math.pow(Math.sin((double)Math.toRadians(x)),2) + Math.pow(Math.cos((double)Math.toRadians(x)),2) - 1.0 < 0.000001) ? true : false );

    }
}