package DZ1_2;

import java.util.Scanner;

public class Dz5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int diskrim = (int) Math.pow(b,2) - 4 * a * c;

        System.out.println((diskrim >= 0) ? "Решение есть" : "Решения нет");

    }
}