package DZ1_2;

import java.util.Scanner;

public class Dz_dop3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String modelePhone = scanner.nextLine();
        int summ = scanner.nextInt();

        System.out.println( modelePhone.matches(".*(iphone|samsung).*") && summ >= 50000 && summ <= 120000 ? "Можно купить" : "Не подходит");
    }
}
