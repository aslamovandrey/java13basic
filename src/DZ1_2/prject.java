package DZ1_2;

import java.util.Scanner;

public class prject {
    static final int DISCONT = 5;
    static final double TAX = 20;
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        // стоимость единицы товара в рублях
        double price = scanner.nextDouble();
        // количество единиц товара
        int count = scanner.nextInt();
        // включение/не включение НДС (20%) в стоимость товара
        String nds;
        do{
            nds = scanner.next();
        } while (!"yes".equalsIgnoreCase(nds) && !"no".equalsIgnoreCase(nds));

        double summ = 0.0;

        if ("yes".equalsIgnoreCase(nds)) {
            if (count>=10){
                summ = proc(price,count,DISCONT,TAX);
            } else {
                summ = proc(price,count,TAX);
            }
        } else {
            if (count>=10){
                summ = proc(price,count,DISCONT);
            } else {
                summ = proc(price,count);
            }
        }

        // Если количество единиц товара больше или равно 10, то действует скидка 5%.
        System.out.println(summ);
    }
    public static double proc(double price,int count){
        double summ = price * count;
        return summ;
    }
    public static double proc(double price,int count,int discont){
        double summ = price * count - price * count * discont / 100;
        return summ;
    }
    public static double proc(double price,int count,int discont, double tax){
        double summ = (price * count + price * count * tax / 100) - price * count * discont / 100;
        return summ;
    }
    public static double proc(double price,int count, double tax){
        double summ = price * count + price * count * tax / 100;
        return summ;
    }
}

