package DZ3_3.task2;
/*
Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
нет. Реализовать метод в цеху, позволяющий по переданной мебели
определять, сможет ли ей починить или нет. Возвращать результат типа
boolean. Протестировать метод.
 */
public class Main {
    public static void main(String[] args) {
        //Сам цех
        BestCarpenterEver carpentry = new BestCarpenterEver();

        Furniture stool = new Stools();
        Furniture table = new Tables();

        System.out.println("Цех чинит объект " + stool.thisIs() + " ? " + carpentry.isCanRepaired(stool));
        System.out.println("Цех чинит объект " + table.thisIs() + " ? " + carpentry.isCanRepaired(table));

    }
}
