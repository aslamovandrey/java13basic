package DZ3_3.task4;

import java.util.ArrayList;

public class Scores {
    private Dog dog;
    private Participant participant;
    private ArrayList<Integer> scores;
    private double avarageScore;

    public Scores(Dog dog, Participant participant, ArrayList<Integer> scores) {
        this.dog = dog;
        this.participant = participant;
        this.scores = scores;
        int temp = 0;
        for (int i = 0; i < scores.size(); i++) {
            temp += scores.get(i);
        }
        this.avarageScore = (int)((double)temp / 3 * 10) / 10.0  ;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public ArrayList<Integer> getScores() {
        return scores;
    }

    public void setScores(ArrayList<Integer> scores) {
        this.scores = scores;
    }

    public double getAvarageScore() {
        return avarageScore;
    }

    @Override
    public String toString() {
        return "Scores{" +
                "dog=" + dog +
                ", participant=" + participant +
                ", scores=" + scores +
                ", avarageScore=" + avarageScore +
                '}';
    }
}
