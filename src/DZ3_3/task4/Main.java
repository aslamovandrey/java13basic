package DZ3_3.task4;

/*
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите кол-во участников, имена хозяев, клички животных и оценки трех судей за выступление");
        int n = scanner.nextInt();

        ArrayList<Participant> owners = new ArrayList<>();
        ArrayList<Dog> dogs = new ArrayList<>();
        ArrayList<Scores> dogsOwnersScores = new ArrayList<>();

        //имена хозяев
        for (int i = 0; i < n; i++) {
            owners.add(new Participant(scanner.next()));
        }
        //клички животных
        for (int i = 0; i < n; i++) {
            dogs.add(new Dog(scanner.next()));
        }
        //оценки
        for (int i = 0; i < n; i++) {
            ArrayList<Integer> scores = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                scores.add(scanner.nextInt());
            }
            dogsOwnersScores.add(new Scores(dogs.get(i),owners.get(i),scores));
        }
        //с компаратором помог Лёха. Не понимаю (пока) как он работает...
        dogsOwnersScores.sort(Comparator.comparingDouble(Scores::getAvarageScore));
        for (Scores all : dogsOwnersScores) {
            System.out.println(all.toString());
        }
        for (int i = dogsOwnersScores.size()-1; i >= dogsOwnersScores.size()-3; i--) {
            System.out.println(dogsOwnersScores.get(i).getParticipant().getName().toString() + ": "
                    + dogsOwnersScores.get(i).getDog().getName().toString() + ", " + dogsOwnersScores.get(i).getAvarageScore());
        }
//        @Testvoid test(){
//            ArrayList<Abc> arr = new ArrayList<>();
//            arr.add(new Abc("1", 0.9));
//            arr.add(new Abc("2", 1.1));
//            arr.add(new Abc("1", 0.8));
//            arr.sort(Comparator.comparing(abc -> abc.doubleValue));
//            arr.sort((a, b) -> Double.compare(b.doubleValue, a.doubleValue));
//            System.out.println(arr);
//        }
    }

}
