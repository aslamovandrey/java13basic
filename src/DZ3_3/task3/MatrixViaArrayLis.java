package DZ3_3.task3;

import java.util.ArrayList;
import java.util.Scanner;

/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
 */
public class MatrixViaArrayLis {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество столбцов ");
        int n = scanner.nextInt();
        System.out.println("Введите количество строк ");
        int m = scanner.nextInt();

        printMatrix(n,m);
    }

    private static void printMatrix(int n, int m) {
        //хм, интересно, получается лист листов
        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            matrix.add(new ArrayList<Integer>());
            for (int j = 0; j < n; j++) {
                matrix.get(i).add(i+j);
                System.out.print(matrix.get(i).get(j) + (matrix.get(i).get(j).toString().length() > 1 ? " " : "  "));
                //System.out.print( matrix.get(i).get(j).toString().length());
            }            System.out.print("\n");
        }
    }
}
