package DZ3_3.task1;
/*
орел (Eagle)
Птицы (Bird) откладывают яйца.
орел летает быстро
 */
public class Eagle extends Bird implements Flying{
    @Override
    public void flying() {
        System.out.println("Орел летает быстро");
    }
}
