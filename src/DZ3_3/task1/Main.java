package DZ3_3.task1;

public class Main {
    public static void main(String[] args) {

        Bat bat = new Bat();
        System.out.println("летучая мышь (Bat)");
        bat.eat();
        bat.sleep();
        bat.wayOfBirth();
        bat.flying();
        System.out.println("");

        Dolphin dolphin = new Dolphin();
        System.out.println("дельфин (Dolphin)");
        dolphin.eat();
        dolphin.sleep();
        dolphin.wayOfBirth();
        dolphin.swimming();
        System.out.println("");

        GoldFish goldFish = new GoldFish();
        System.out.println("золотая рыбка (GoldFish)");
        goldFish.eat();
        goldFish.sleep();
        goldFish.wayOfBirth();
        goldFish.swimming();
        System.out.println("");

        Eagle eagle = new Eagle();
        System.out.println("орел (Eagle)");
        eagle.eat();
        eagle.sleep();
        eagle.wayOfBirth();
        eagle.flying();
    }
}
