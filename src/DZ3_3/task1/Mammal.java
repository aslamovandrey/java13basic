package DZ3_3.task1;

import DZ3_3.task1.Animals;

/*
животные умеют по-разному рождаться (wayOfBirth):
● Млекопитающие (Mammal) живородящие.
 */
public abstract class Mammal extends Animals {
    @Override
    void wayOfBirth() {
        System.out.println("Млекопитающие (Mammal) живородящие");
    }
}
