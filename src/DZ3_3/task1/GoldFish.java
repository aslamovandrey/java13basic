package DZ3_3.task1;
/*
золотая рыбка (GoldFish)
Рыбы (Fish) мечут икру
золотая рыбка плавает медленно
 */
public class GoldFish extends Fish implements Swimming {
    @Override
    public void swimming() {
        System.out.println("Золотая рыбка плавает медленно");
    }
}
