package DZ3_3.task1;

import DZ3_3.task1.Animals;

/*
животные умеют по-разному рождаться (wayOfBirth):

● Рыбы (Fish) мечут икру.

 */
public abstract class Fish extends Animals {
    @Override
    void wayOfBirth() {
        System.out.println("Рыбы (Fish) мечут икру");
    }
}
