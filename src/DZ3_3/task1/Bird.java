package DZ3_3.task1;

import DZ3_3.task1.Animals;

/*
животные умеют по-разному рождаться (wayOfBirth):


● Птицы (Bird) откладывают яйца.
 */
public abstract class Bird extends Animals {
    @Override
    void wayOfBirth() {
        System.out.println("Птицы (Bird) откладывают яйца");
    }
}
