package DZ3_3.task1;
/*
летучая мышь (Bat)
летающие животные
Млекопитающие
летает  летучая мышь медленно
 */
public class Bat extends Mammal implements Flying {
    @Override
    public void flying() {
        System.out.println("Летучая мышь медленно летает");
    }
}
