package DZ3_3.task1;
/*
дельфин (Dolphin)
Млекопитающие (Mammal) живородящие
плавающие (Swimming)
Дельфин плавает быстро
 */
public class Dolphin extends Mammal implements Swimming{
    @Override
    public void swimming() {
        System.out.println("Дельфин плавает быстро");
    }
}
