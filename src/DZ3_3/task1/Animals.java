package DZ3_3.task1;
/*
Все животные одинаково едят и спят (предположим), и никто из животных не
должен иметь возможности делать это иначе.
Еще животные умеют по-разному рождаться (wayOfBirth):
 */
public abstract class Animals {
    final void eat(){
        System.out.println("Ест");
    }
    final void sleep(){
        System.out.println("Спит");
    }
    abstract void wayOfBirth() ;
}
