package DZ2_2;

import java.util.Scanner;

public class Dz10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(recursWhisSpaseElement(Integer.toString(n)).substring(1));
        //recursWhisSpaseElementNumber(n);
    }

    private static String recursWhisSpaseElement(String s) {
        if (s.isEmpty()) {
            return s;
        }
        else {

            return recursWhisSpaseElement(s.substring(1)) + " " + s.charAt(0);
        }
    }

   /* private static void recursWhisSpaseElementNumber(int n) {
        if (n < 10) {
            System.out.print(n);
        }
        else {
            System.out.print(n%10 + " ");
           recursWhisSpaseElementNumber(n / 10);
        }
    }
*/
}
