package DZ2_2;

import java.util.Scanner;

public class Dz5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] arr_n = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr_n[i][j] = scanner.nextInt();
            }
        }
        boolean flag = true;
        for (int i = 0, l = n-1; i < n; i++, l--) {
            for (int j = 0, s = n-1; j < n; j++, s--) {
                if (arr_n[i][j] != arr_n[s][l]) {
                    flag = false;
                    break;
                }
            }
        }
        System.out.println(flag);

    }
}
