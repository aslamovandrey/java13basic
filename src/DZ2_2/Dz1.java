package DZ2_2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Dz1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] arr_n = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr_n[i][j] = scanner.nextInt();
            }
        }
        int[] arrRes = new int[m];
        for (int i = 0; i < m; i++) {
            Arrays.sort(arr_n[i]);
            arrRes[i] = arr_n[i][0];
        }
        for (int i = 0; i < arrRes.length; i++) {
            System.out.print(arrRes[i] + " ");
        }

    }
}
