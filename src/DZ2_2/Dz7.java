package DZ2_2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Dz7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        String[] names = new String[n];
        String[] dogs = new String[n];
        double[][] scores = new double[n][3];

        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine()) {
            if (scanner.hasNextDouble()) break;
            String line = scanner.nextLine();
            if (line.isEmpty()) {
                break;
            }
            sb.append(line).append("\n");
        }
        sb.deleteCharAt(sb.length() - 1);
        String s = sb.toString();

        String[] in = s.split("\n");

        int p = 0;
        for (int i = 0; i < n; i++) {
            names[i] = in[i];
            dogs[i] = in[i + 4];
        }

        for (int i = 0; i < 4; i++) {
            for (int h = 0; h < 3; h++) {
                scores[i][h] = scanner.nextDouble();
            }
        }

        double[] scoresAverage = new double[n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < 3; k++) {
                scoresAverage[i] += scores[i][k] / 3;
            }
        }

        int index = 0;
        for (int i = 0; i < 3; i++) {
            double max = scoresAverage[i];
            for (int k = 0; k < n; k++) {
                if (scoresAverage[k] >= max) {
                    max = scoresAverage[k];
                    index = k;
                }
            }
            System.out.println(names[index] + ": " + dogs[index] + ", " + ((int) (scoresAverage[index] * 10)) / 10.0);
            scoresAverage[index] = -1;
        }
    }

}
