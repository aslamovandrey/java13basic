package DZ2_2;

import java.util.Scanner;

public class Dz8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(recursSummElement(n));
    }

    private static int recursSummElement(int n) {
        if (n == 0)
            return 0;
        else
            return n % 10 + recursSummElement(n/10);
    }
}
