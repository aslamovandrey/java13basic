package DZ2_2;

import java.util.Scanner;

public class Dz6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 8;
        int m = 4;

        int[][] arr_n = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr_n[i][j] = scanner.nextInt();
            }
        }
        int tmpRes;
        boolean resault = true;

        for (int i = 0; i < m; i++) {
            tmpRes = 0;
            for (int j = 1; j < n; j++) {
                tmpRes += arr_n[j][i];
            }
            if (arr_n[0][i] < tmpRes){
                resault = false;
                break;
            }

        }
        System.out.println(resault == true ? "Отлично" : "Нужно есть поменьше");
    }
}
