package DZ2_2;

import java.util.Scanner;

public class Dz3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        String[][] arr_n = new String[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                hodKonem(i ,j , n, arr_n, "0");
            }
        }
        hodKonem(x1 ,y1 , n, arr_n, "K");
        int aHod[][] = {
                {1, -2},
                {2, -1},
                {2, 1},
                {1, 2},
                {-1, 2},
                {-2, 1},
                {-2, -1},
                {-1, -2}
        };
        for (int i = 0; i < 8; i++) {
             hodKonem(x1 + aHod[i][0],y1 + aHod[i][1], n, arr_n, "X");
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n-1){
                    System.out.print(arr_n[i][j]);
                } else {
                    System.out.print(arr_n[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    private static void hodKonem(int x1, int y1, int n, String[][] arr, String s) {
        if (x1 >= 0 && x1 < n && y1 >= 0 && y1 < n) {
            arr[y1][x1] = s;
        }
    }
}
