package DZ2_2;

import java.util.Scanner;

public class Dz4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] arr_n = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr_n[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();

        int x = 0;
        int y = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(arr_n[i][j] == p) {
                    x=i;
                    y=j;
                }
            }
        }
        int[][] arr_m = new int[n-1][n-1];
        for (int i = 0, l = 0; i < n; i++) {
            for (int j = 0, s = 0; j < n; j++) {
                if(i != x && j != y){
                    arr_m[l][s] = arr_n[i][j];
                    s++;
                }
            }
            if(i != x) l++;

        }

        for (int i = 0; i < arr_m.length; i++) {
            for (int j = 0; j < arr_m.length; j++) {
                if (j == arr_m.length-1){
                    System.out.print(arr_m[i][j]);
                } else {
                    System.out.print(arr_m[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

}
