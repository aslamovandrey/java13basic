package DZ2_2;

import java.util.Scanner;

public class Dz9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(recursWhisSpaseElement(Integer.toString(n), Integer.toString(n).length()));
    }

    private static String recursWhisSpaseElement(String s, int l) {
        if (l == 1) {
            return s;
        }
        else {
            l--;
            return recursWhisSpaseElement(s.substring(0,l) + " " + s.substring(l), l);
        }
    }
}