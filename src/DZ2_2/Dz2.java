package DZ2_2;

import java.util.Scanner;

public class Dz2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        int[][] arr_n = new int[n][n];
        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                if (i==3 && j==2) {
                    arr_n[i][j] = 0;
                }
                else {
                    arr_n[i][j] = 1;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n-1){
                    System.out.print(arr_n[i][j]);
                } else {
                    System.out.print(arr_n[i][j] + " ");
                }
            }
            System.out.println();
        }
    }
}
