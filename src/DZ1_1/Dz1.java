package DZ1_1;

import java.util.Scanner;

public class Dz1 {
    public static void main(String[] args) {
        double radius;

        Scanner input = new Scanner(System.in);

        radius = input.nextDouble();
        System.out.println( 4.0/3.0 * Math.PI * Math.pow(radius,3));
    }

}
