package DZ1_1;

import java.util.Scanner;

public class Dz5 {
    public static void main(String[] args) {
        final double SM_IN_DUIM = 2.54;
        int duim;
        Scanner input = new Scanner(System.in);

        duim = input.nextInt();
        System.out.println(duim * SM_IN_DUIM);

    }
}
