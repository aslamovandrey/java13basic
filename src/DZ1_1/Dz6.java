package DZ1_1;

import java.util.Scanner;

public class Dz6 {
    public static void main(String[] args) {
        final double KM_IN_MILE = 1.60934;
        double count;
        Scanner input = new Scanner(System.in);

        count = input.nextInt();
        System.out.println(count / KM_IN_MILE);

    }
}

