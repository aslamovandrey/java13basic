package DZ1_1;

import java.util.Scanner;

public class Dz4 {
    public static void main(String[] args) {
        final double HOURS_PER_DAY = 24;
        final double MINS_PER_HOURS = 60;
        //final double MIL_PER_SEK = 1000;
        double miliSeconds;

        Scanner input = new Scanner(System.in);
        miliSeconds = input.nextDouble();

        System.out.println((int)(miliSeconds / MINS_PER_HOURS / MINS_PER_HOURS) + " " + (int)(miliSeconds / MINS_PER_HOURS % 60 ));
    }
}
