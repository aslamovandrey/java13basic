package DZ3_2;

import week9.oop3.arrlist.Car;

public class Main {
    public static void main(String[] args) {
        Library pitersLibrary = new Library();
        //создать объекты книг
        Book book1 = new Book("Russian history","Ivanov I.I.");
        Book book4 = new Book("China history","Ivanov I.I.");
        Book book2 = new Book("Geometry","Petrov P.P.");
        Book book3 = new Book("Literature","Sidorov S.S.");

        //добавить книга в библиотеку
        pitersLibrary.addBookInLib(book1);
        pitersLibrary.addBookInLib(book2);
        pitersLibrary.addBookInLib(book3);
        pitersLibrary.addBookInLib(book4);

        //создать объекты посетителей
        User user1 = new User("Gevirg");
        User user2 = new User("Dmitriy");

        System.out.println("    # Список книг в библиотеке #");
        pitersLibrary.printList();
        System.out.println("    # -- # \n");


        pitersLibrary.delBookInLib(book2);
        System.out.println("    # Список книг в библиотеке после удаления #");
        pitersLibrary.printList();
        System.out.println("    # -- # \n");

        System.out.println("    # Книга найденная по названию #");
        System.out.println(pitersLibrary.getBookForName("Literature"));
        System.out.println("    # -- # \n");

        System.out.println("    # Книга или список книг найденные по автору #");
        pitersLibrary.getListBookForAuthor("Ivanov I.I.").forEach(System.out::println);
        System.out.println("    # -- # \n");

        System.out.println("    # Выдать книгу читателю #");
        pitersLibrary.gaveBookToUser("Russian history",user1);
        System.out.println("    # Список книг в библиотеке #");
        pitersLibrary.printList();
        System.out.println(user1);
        System.out.println("    # -- # \n");

        System.out.println("    # Вернуть книгу от читателю #");
        pitersLibrary.returnBookFromUser(user1);
        System.out.println(user1);
        System.out.println(user2);

    }
}
