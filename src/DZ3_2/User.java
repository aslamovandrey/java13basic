package DZ3_2;

public class User {
    private String name;
    private Integer idUser = null;
    private Book bookOnHand;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Book isBookOnHand() {
        return bookOnHand;
    }

    public void setBookOnHand(Book bookOnHand) {
        this.bookOnHand = bookOnHand;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", idUser=" + idUser +
                ", bookOnHand=" + bookOnHand +
                '}';
    }
}
