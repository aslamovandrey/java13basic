package DZ3_2;

import week9.oop3.arrlist.Car;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Library {

    private ArrayList<Book> bookList;

    public Library() {
        this.bookList = new ArrayList<>();
    }
    public void addBookInLib(Book book){    //Добавить новую книгу в библиотеку
        boolean flag = true;
        Iterator<Book> iterator =  bookList.iterator();
        while (iterator.hasNext()){
            Book iterBook = iterator.next();
            if (iterBook.getName().equals(book.getName())) {
                flag = false;
            }
        }
        if (flag) bookList.add(book);
    }
    public void delBookInLib(Book book){     //Удалить книгу из библиотеки по названию
        boolean flag = false;
        Iterator<Book> iterator =  bookList.iterator();
        while (iterator.hasNext()){
            Book iterBook = iterator.next();
            if (iterBook.getName().equals(book.getName()) && iterBook.isInLibrary()) {
                flag = true;
            }
        }
        if (flag) bookList.remove(book);
    }

    public Book getBookForName(String searchName){  //Найти и вернуть книгу по названию.
        Book res = null;
        for (Book e : bookList) {
            if (e.getName().equals(searchName)){
                res = e;
            }
        }
        return res;
    }

    public ArrayList<Book> getListBookForAuthor(String searchAuthor){  //Найти и вернуть список книг по автору.
        ArrayList<Book> listByAuthor = new ArrayList<>();
        for (Book e : bookList) {
            if (e.getAuthor().equals(searchAuthor)){
                listByAuthor.add(e);
            }
        }
        return listByAuthor;
    }

    public void gaveBookToUser(String searchBookName, User user){   //Одолжить книгу посетителю по названию,
        if (getBookForName(searchBookName) instanceof Book && user.isBookOnHand() == null && getBookForName(searchBookName).isInLibrary()){
            if (user.getIdUser() == null){
                Random gen = new Random();
                user.setIdUser(gen.nextInt(1000));
            }
            user.setBookOnHand(getBookForName(searchBookName));
            getBookForName(searchBookName).setInLibrary(false);

        }
    }

    public void returnBookFromUser(User user){
        if (user.isBookOnHand() instanceof Book){
            user.isBookOnHand().setInLibrary(true);
            user.setBookOnHand(null);
        }
    }
    public void printList(){    //Вывод в консоль для удобства
        this.bookList.forEach(System.out::println);
    }
}
