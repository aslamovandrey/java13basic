package fourthweekpractice;

public class ExamplePrePostInc {
    public static void main(String[] args) {
        int i = 1;
        int a = ++i;

//        int temp = i;
//        i = i + 1;
//        a = temp;

        System.out.println(i);
        System.out.println(a);
    }
}
