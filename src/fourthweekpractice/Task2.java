package fourthweekpractice;
/*
Даны два числа m и n.
Найти произведение чисел в диапазоне между m и n включительно.
(m < 14 и n < 14), m < n

4 12 -> 79833600
3 7 -> 2520

 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int res = 1;
        for (int i = m; i <= n ; i++){
            res = res * i;
        }
        System.out.println(res);
    }
}
