package fourthweekpractice;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int res = 0;
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) != ' ')
                res++;
        }
        System.out.println(res);
    }
}
