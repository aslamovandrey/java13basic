package fifthweekpractice;
/*
  На вход подается число N - длина массива.
     Затем передается массив строк длины N.
     После этого - число M.

     Сохранить в другом массиве только те элементы, длина строки которых
     не превышает M.

     Входные данные:
     5
     Hello
     good
     to
     see
     you
     4

     Выходные данные:
     good to see you
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] arr = new String[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }

        int m = scanner.nextInt();
        int k = 0;
        String[] res = new String[n];
        for (String s : arr) {
            if (s.length() <= m) {
                res[k++] = s;
            }
        }
    }
}
