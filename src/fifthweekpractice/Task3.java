package fifthweekpractice;

import java.util.Scanner;
/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.

Проверить, является ли он отсортированным массивом строго по убыванию.
Если да, вывести true, иначе вывести false.

5
5 4 3 2 1
->
true

2
43 46
->
false

3
5 5 5
->
false

 */

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
//        boolean flag = true;
//        for (int i = 1; i < n; i++) {
//            if (arr[i-1]<=arr[i]){
//                flag= false;
//                break;
//            }
//        }
        System.out.println(checkData(arr));
    }
    public static boolean checkData (int[] inArr) {
        for (int i = 0; i < inArr.length-1; i++) {
            if (inArr[i] <= inArr[i + 1]) {
                return false;
            }
        }
        return true;
    }
}
