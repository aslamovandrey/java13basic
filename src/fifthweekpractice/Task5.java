package fifthweekpractice;
/*
 На вход подается число N — длина массива.
 Затем передается отсортированный по возрастанию массив целых различных чисел из N элементов.
 После этого число M.


Найти в массиве все пары чисел, которые в сумме дают число M и вывести их на экран.
Если таких нет, то вывести -1.

5
1 2 3 4 7
5
->
1 4
2 3
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean flag = false;

        int[] arr  =new int[n];
        for (int i = 0; n < n; n++) {
            arr[i] = scanner.nextInt();;
        }
        int m = scanner.nextInt();

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] + arr[j] == m) {
                    System.out.println(arr[i] + " " + arr[j]);
                    flag = true;
                }
            }
        }
        if (!flag) {
            System.out.println(-1);
        }

    }
}
