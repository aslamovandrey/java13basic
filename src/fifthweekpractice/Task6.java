package fifthweekpractice;
/*
     На вход подается число N - длина массива.
     Затем подается массив целых чисел из N элементов.

     Нужно циклически сдвинуть элементы на 1 влево.

     Входные данные:
     5
     1 2 3 4 7
     Выходные данные:
     2 3 4 7 1
     */

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int temp = arr[n];
        for (int i = 1; i < n-1; i++) {
            arr[i] = arr[i-1];
        }
        arr[0] = temp;
        System.out.println(Arrays.toString(arr));
    }
}
