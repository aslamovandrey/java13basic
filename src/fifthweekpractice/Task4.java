package fifthweekpractice;
/*
   На вход подается два отсортированных массива.
   Нужно создать отсортированный третий массив,
   состоящий из элементов первых двух.

   Входные данные:
   5
   1 2 3 4 7

   2
   1 6

   Выходные данные:
   1 1 2 3 4 6 7
    */

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i = 0; i < n; i++) {
            arr1[i] = scanner.nextInt();

        }
        int k = scanner.nextInt();
        int[] arr2 = new int[k];
        for (int i = 0; i < k; i++) {
            arr2[i] = scanner.nextInt();
        }

        mergeTwuArr(arr1, arr2);
        mergeTwuArrCopy(arr1,arr2);
        mergeTwoArrays(arr1,arr2);

    }

    /**
     * Метод делает слияние двух массивов в третий
     * @param arr1 - первый
     * @param arr2 - второй
     */
    public static void mergeTwuArr(int[] arr1, int[] arr2) {
        int[] mergearr = new int[arr1.length + arr2.length];

        int pos = 0;
        for (int element : arr1) {
            mergearr[pos] = element;
            pos++;
        }

        for (int element : arr2) {
            mergearr[pos] = element;
            pos++;
        }
        Arrays.sort(mergearr);
        System.out.println(Arrays.toString(mergearr));
    }
    public static void mergeTwuArrCopy (int[] arr1, int[] arr2) {
        int[] mergearr = new int[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, mergearr, 0 , arr1.length);
        System.arraycopy(arr2, 0 , mergearr, arr1.length, arr2.length);
        Arrays.sort(mergearr);
        System.out.println(Arrays.toString(mergearr));
    }
    public static void mergeTwoArrays(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;

        //обход двух массивов
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergedArray[k++] = arr1[i++];
            } else {
                mergedArray[k++] = arr2[j++];
            }
        }
            //сохраняем оставшиеся элементы первого массива
            while (i < arr1.length) {
                mergedArray[k++] = arr1[i++];
            }

            //сохраняем оставшиеся элементы второго массива
            while (j < arr2.length) {
                mergedArray[k++] = arr2[j++];
            }
            System.out.println(Arrays.toString(mergedArray));
        }

}
