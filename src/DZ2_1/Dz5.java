package DZ2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Dz5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr_n = new int[n];
        for (int i = 0; i < n; i++) {
            arr_n[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] arrresult = new int[n];

        for (int i = 0; i < n; i++) {
            if(i<m){
                arrresult[i] = arr_n[n-m+i];
            } else {
                arrresult[i] = arr_n[i-m];
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.print(arrresult[i]+ " ");
        }
    }
}
