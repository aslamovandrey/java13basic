package DZ2_1;

import java.util.Scanner;

public class Dz10 {
    public static void main(String[] args) {
        gameNumber();
    }

    private static void gameNumber() {
        int m = (int)(Math.random() * 1000);
        System.out.println("!!! ИГРА - УГАДАЙ ЧИСЛО !!!");
        System.out.println("Компьютер загадал чесло от 0 до 1000, отгадай ! ");
        Scanner scanner = new Scanner(System.in);
        int inputNumberUser = 0;
        //System.out.println(m);
        do {
            System.out.print("Введите ваше число: ");
            inputNumberUser = scanner.nextInt();
            if (m == inputNumberUser){
                System.out.println("Победа!");
            } else if (m < inputNumberUser) {
                System.out.println("Это число больше загаданного." );
            } else if (m > inputNumberUser) {
                System.out.println("Это число меньше загаданного." );
            } else if (inputNumberUser < 0) {
                break;
            }

        } while (m != inputNumberUser);
    }
}
