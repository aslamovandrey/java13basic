package DZ2_1;

import java.util.Scanner;

public class Dz6 {
    public static void main(String[] args) {

        String[] arrStr = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                 "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int index;

        for (int i = 0; i < s.length(); i++) {
            index = (int)(s.charAt(i) - 1040);
            System.out.print(arrStr[index] + " ");
        }

    }
}
