package DZ2_1;

import java.util.Scanner;

public class Dz9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] arr = new String[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }
        int idx = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i].equals(arr[j]) && i != j) {
                    idx = j;
                }
            }

        }
        System.out.println(arr[idx]);
}
}
