package DZ2_1;

import java.util.Scanner;

public class Dz3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr_n = new int[n];
        for (int i = 0; i < n; i++) {
            arr_n[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();

        int index = 0;
        for (int i = 0; i < n; i++) {
            if (arr_n[i] > m) {
                index = i;
                break;
            } else {
                index = arr_n.length-1;
            }
        }
        System.out.println(index);
    }
}
