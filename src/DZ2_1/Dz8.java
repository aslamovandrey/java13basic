package DZ2_1;

import java.util.Scanner;

public class Dz8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr_n = new int[n];
        for (int i = 0; i < n; i++) {
            arr_n[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int temp = Math.abs(arr_n[0] - m);
        int res = arr_n[0];
        for (int i = 0; i < n; i++) {
            if(Math.abs(arr_n[i] - m) < temp || Math.abs(arr_n[i] - m) == temp && arr_n[i] < res ){
                res = arr_n[i];
            }

        }
        System.out.println(res);
    }
}
