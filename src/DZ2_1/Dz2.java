package DZ2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Dz2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr_n = new int[n];
        for (int i = 0; i < n; i++) {
            arr_n[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] arr_m = new int[m];
        for (int i = 0; i < m; i++) {
            arr_m[i] = scanner.nextInt();
        }

        System.out.println(Arrays.equals(arr_n,arr_m));

    }
}
