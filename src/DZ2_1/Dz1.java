package DZ2_1;

import java.util.Scanner;

public class Dz1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] arr = new double[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextDouble();
        }
        System.out.println(average(arr, n));
    }
    public static double average(double arr[], int n){
        double summ = 0.0;
        for (double e: arr) {
            summ += e;
        }

        return summ / n;
    }
}
