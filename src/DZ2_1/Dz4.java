package DZ2_1;

import java.util.Scanner;

public class Dz4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr_n = new int[n];
        for (int i = 0; i < n; i++) {
            arr_n[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++) {
            int sravniParam = arr_n[i];
            int counter = 0;

            for (int j = 0; j < n; j++) {
                if (sravniParam == arr_n[j]){
                    counter++;
                }
            }
            if (i == 0 || arr_n[i] != arr_n[i-1]) {
                System.out.println(counter + " " + sravniParam);
            }
        }
    }
}
