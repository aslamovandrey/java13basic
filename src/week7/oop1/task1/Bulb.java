package week7.oop1.task1;
/*
Реализовать класс “Лампа”. Методы:
включить лампу
выключить лампу
получить текущее состояние
 */

public class Bulb {
    private boolean toggle;

    public Bulb(boolean toggle){
        this.toggle = toggle;
    }
     Bulb(){
        //System.out.println(toggle);
        this.toggle = false;
    }
    public void tutnOn(){
        this.toggle = true;
    }

    public void tutnOff(){
        this.toggle = false;
    }

    public boolean isShining() {
        return this.toggle;
    }

}
