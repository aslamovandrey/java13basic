package week7.oop1.task1;

public class Chandelier {
    public Bulb[] chandelier;



    public Chandelier(int countBulb){
        chandelier = new Bulb[countBulb];

        for (int i = 0; i< countBulb; i++){
            chandelier[i] = new Bulb();
        }

    }
    public void turnOn(){
        for (Bulb bulb : chandelier) {
            bulb.tutnOn();
        }
    }
    public void turnOff(){
        for (Bulb bulb : chandelier) {
            bulb.tutnOff();
        }
    }

    public boolean isShining() {
        return chandelier[0].isShining();
    }

}
