package week7.oop1.task2;
/*

Реализовать класс “Термометр”.
Необходимо иметь возможность создавать инстанс класса с текущей температурой и
получать значение в фаренгейте и в цельсии.

книга "Чистый код" Роберт Мартин


 */

public class Thermometr {
    private double tempCelsius;
    private double tempFarengate;

    public Thermometr(double currentTemperature, String temperaturUnit){
        if (temperaturUnit.equals("C")){
            tempCelsius = currentTemperature;
            tempFarengate = fromCelToFar(currentTemperature);
        } else if (temperaturUnit.equals("F")) {
            tempFarengate = currentTemperature;
            tempCelsius = fromFarToCels(currentTemperature);
        }
        else {
            System.out.println("температура не распознана");
            //throw new UnsupportedOperationException("температура не распознана");
            tempCelsius = currentTemperature;
            tempFarengate = fromCelToFar(currentTemperature);
        }

    }


    public double getTempCelsius() {
        return tempCelsius;
    }

    public double getTempFarengate() {
        return tempFarengate;
    }

    private double fromCelToFar(double currentTemperature){
        return currentTemperature * 1.8 +32;
    }
    private double fromFarToCels(double currentTemperature){
        return (currentTemperature - 32) * 1.8;
    }
}
