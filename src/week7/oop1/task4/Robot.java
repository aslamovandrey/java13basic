package week7.oop1.task4;
/*
Робот. Команды повернуть влево, повернуть вправо, идти.
Несколько конструкторов, хранение координат, вывод потом координат на экран.
 */

public class Robot {
    private int x;
    private int y;
    private int discrition; //0 -- up, 1 -- right, 2 -- bottom, 3 -- left

    public Robot(){
        this.x = 0;
        this.y = 0;
        this.discrition = 0;
    }

    public Robot(int x,int y){
        this.x = x;
        this.y = y;
        this.discrition = 0;
    }
    public Robot(int x,int y, int discrition){
        this.x = x;
        this.y = y;
        this.discrition = discrition;
    }

    public void go() {
        switch (discrition){
            case 0 -> y++;
            case 1 -> x++;
            case 2 -> y--;
            case 3 -> x--;
        }
    }

    public void ternLeft(){
        this.discrition = (discrition -1) % 4;
    }
    public void ternRight(){
        this.discrition = (discrition +1) % 4;
    }
    public void printCoordinates(){
        System.out.println("x and y " + x + " " + y);
    }
}
