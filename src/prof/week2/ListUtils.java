package prof.week2;

import java.util.List;

public class ListUtils {
    public static <T> int countIf(List<T> from, T elem){
        int counter = 0;
            for( T e : from){
                if (e.equals(elem)){
                    counter++;
                }
            }
        return counter;
    }
    private ListUtils(){

    }
}
