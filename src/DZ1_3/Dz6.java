package DZ1_3;

import java.util.Scanner;

public class Dz6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int[] moneyNom = {8, 4, 2, 1};
        int[] moneyCou = {0, 0, 0, 0};
        int ost = 0;
        for (int i = 0; i < moneyNom.length; i++) {
            if (m >= moneyNom[i]) {
                moneyCou[i] = m / moneyNom[i];
                m = m % moneyNom[i];
            }
        }
        for (int i = 0; i < moneyCou.length; i++) {
            System.out.print(moneyCou[i] + " ");
        }
    }
}
