package DZ1_3;

import java.util.Scanner;

public class Dz10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++ ) {
            for (int j = 1; j<= n+n-1 ; j++) {
                if (j > n - i && j < n + i) {
                    System.out.print("#");
                } else if(j <= n - i) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        for (int i = 1; i <= n+n-1; i++) {
            if (i == n ) {
                System.out.print("|");
            } else if (i < n ){
                System.out.print(" ");
            }
        }
        System.out.println();
    }
}