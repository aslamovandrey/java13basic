package DZ1_3;

import java.util.Scanner;

public class Dz7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') count++;
        }
        System.out.println(count);
    }
}
