package DZ1_3;

import java.util.Scanner;

public class Dz8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int sum = 0;

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        for (int e : arr) {
            if(e > p) sum += e;
        }
        System.out.println(sum);
    }
}
