package DZ1_3;

import java.util.Scanner;

public class Dz3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int res = 0;
        for (int i = 1; i <= n; i++) {
            res += Math.pow(m,i);
        }
        System.out.println(res);
    }
}
