package HomeworkToCheck;

import java.util.Scanner;

public class Dz2_2_10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        // 1 способ, через String
        System.out.println(recursWhisSpaseElement(Integer.toString(n)).substring(1));
        // 2 способ, через int
        recursWhisSpaseElementNumber(n);
    }

    /**
     * Рекурсия возвращает строку справа налево
     * @param s
     * @return
     */
    private static String recursWhisSpaseElement(String s) {
        if (s.isEmpty()) {
            return s;
        }
        else {

            return recursWhisSpaseElement(s.substring(1)) + " " + s.charAt(0);
        }
    }

    /**
     * Рекурсия выводит в System.out цифры числа справа налево
     * @param n
     */
    private static void recursWhisSpaseElementNumber(int n) {
        if (n < 10) {
            System.out.print(n);
        }
        else {
            System.out.print(n%10 + " ");
            recursWhisSpaseElementNumber(n / 10);
        }
    }
}
