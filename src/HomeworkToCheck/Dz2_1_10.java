package HomeworkToCheck;
//Необходимо реализовать игру

import java.util.Scanner;

public class Dz2_1_10 {
    public static void main(String[] args) {
        gameNumber();
    }

    /**
     * Метод реализует алгоритм игры "угадай число"
     */
    private static void gameNumber() {
        int m = (int)(Math.random() * 1000);
        System.out.println("!!! ИГРА - УГАДАЙ ЧИСЛО !!!");
        System.out.println("Компьютер загадал чесло от 0 до 1000, отгадай ! ");
        Scanner scanner = new Scanner(System.in);
        int inputNumberUser;
        //System.out.println(m);

        do {
            System.out.print("Введите ваше число: ");
            inputNumberUser = scanner.nextInt();
            if (m == inputNumberUser){
                System.out.println("Победа!");
            } else if (inputNumberUser < 0) {
                System.out.println("Выход");
                break;
            } else if (m < inputNumberUser) {
                System.out.println("Это число больше загаданного." );
            } else {
                System.out.println("Это число меньше загаданного." );
            }

        } while (m != inputNumberUser);
    }
}
