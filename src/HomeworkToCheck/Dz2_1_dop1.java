package HomeworkToCheck;
// Программа герерирующая безопасный пароль

import java.util.Scanner;

public class Dz2_1_dop1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;

        // цикл, пока введенная длинна пароля булет безопасной
        do {
            System.out.print("Введи длинну пароля: ");
            n = scanner.nextInt();
            if (n < 8) {
                System.out.println("Пароль с " +  n + " символами небезопасен");
            }
        } while (n < 8);

        //Сам генератор
        String sFull = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_*-";
        String pass = "";
        for (int i = 0; i < n; i++) {
            switch (i % 5) {
                case 0 :
                    pass += sFull.charAt(26 + (int)(Math.random()*25));
                    break;
                case 1 :
                    pass += sFull.charAt((int)(Math.random()*25));
                    break;
                case 2 :
                    pass += sFull.charAt(52 + (int)(Math.random()*9));
                    break;
                case 3 :
                    pass += sFull.charAt(62 + (int)(Math.random()*3));
                    break;
                case 4 :
                    pass += sFull.charAt((int)(Math.random()*64));
                    break;
            }
        }
        System.out.println(pass);
    }
}
