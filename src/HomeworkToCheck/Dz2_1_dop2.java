package HomeworkToCheck;
//Решить задачу 7 основного дз за линейное время
import java.util.Scanner;

public class Dz2_1_dop2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr_n = new int[n];
        for (int i = 0; i < n; i++) {
            arr_n[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            arr_n[i] *= arr_n[i];
        }
        //Сложность O(n)
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr_n[i] > arr_n[j]){
                    int temp = arr_n[i];
                    arr_n[i] = arr_n[j];
                    arr_n[j] = temp;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.print(arr_n[i] + " ");
        }
    }
}
