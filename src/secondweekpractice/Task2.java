package secondweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        if (n % 2 == 0 && n >= 0) {
            System.out.println( "Четное больше или равно 0");
        }
        else if (n % 2 == 0 && n < 0) {
            System.out.println( "Четное меньше 0");
        }
    }
}
