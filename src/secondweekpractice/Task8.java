package secondweekpractice;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sim = scanner.next();
        char c = sim.charAt(0);

        if (c>= 'a' && c <= 'z'){
            System.out.println((char) (c+ ('A' - 'a')));
        } else {
            System.out.println((char) (c- ('A' - 'a')));
        }
    }
}
