package secondweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        if (n == 1) System.out.println("VIP = 125");
//        if (n == 2) System.out.println("Premium = 110");
//        if (n == 3) System.out.println("Standart = 100");

        switch (n) {
            case 1:
                System.out.println("VIP = 125");
                break;
            case 2:
                System.out.println("Premium = 110");
                break;
            case 3:
                System.out.println("Premium = 110");
                break;
            default:
                System.out.println("Введите корректный номер");
                break;
        }
    }
}